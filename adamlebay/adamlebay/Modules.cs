﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adamlebay
{
    //this class is used to hold the module details. It contains two dictionaries to hold the student results and to hold the students on the module
    public class Modules
    {

        private string name;
        private string code;
        private Staff leader;
        //the key is the students matric number as they cannot be enrolled twice on the course
        private Dictionary<int, Students> studentsOnModule = new Dictionary<int, Students>();
        private Dictionary<int, results> studentGrades = new Dictionary<int, results>();

        //checks for blank values in textbox
        //@param stringIN: takes in a string from a textbox passed from setters
        //returns true if there is a value, false if no value
        private bool checkNotBlank(string stringIN)
        {
            if (stringIN == "")
            {
                return false;
            }
            return true;
        }
        //this is the setter to set the module name. It returns a boolean value if the data entered is incorrect 
        public bool setName(string nameIN)
        {
            //uses the checkNotBlank method to see if the value passed is empty
            if (checkNotBlank(nameIN))
            {
                name = nameIN;
                return true;
            }
            //return false if the name is not set
            return false;
        }
        public bool setCode(string codeIN)
        {
            if (checkNotBlank(codeIN))
            {
                //checks the global dictionary for the module code to see if it already exists
                if (listHolder.modulesDict.ContainsKey(codeIN))
                {
                    return false;
                }
                else
                {
                    code = codeIN;
                    return true;
                }
            }
            return false;
        }
        public string returnName()
        {
            return name;
        }
        public string returnCode()
        {
            return code;
        }
        //this is used to set the module leader. It is used to originally set a leader upon module creation and to change the module leader
        //returns a bool value if the data 
        public bool setLeader(string leaderPnoIn)
        {
            
            int result;
            bool b = Int32.TryParse(leaderPnoIn, out result);

            if (b)
            {
                if (listHolder.staffDict.ContainsKey(result))
                {
                    leader = listHolder.staffDict[result];
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }
        //returns the full dictionary, mostly used for testing purposes
        public Dictionary<int,Students> returnStudentsOnModule()
        {
            return studentsOnModule;
        }
        /// <summary>
        ///method used to add a student into the dictionary
        /// </summary>
        /// <param name="matric">takes in the student matric number for the key</param>
        /// <param name="studentIN">takes in a student object to fill the dictionary</param>
        /// <returns>boolean true if sucess false if otherwise</returns>
        public bool addStudent(int matric, Students studentIN)
        {
            if (studentsOnModule.ContainsKey(matric))
            {
                return false;
            }
            else
            {
                if(listHolder.studentDic.ContainsKey(matric))
                {
                    studentsOnModule.Add(matric, studentIN);
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
        }
        /// <summary>
        /// removes a student from the dictionary
        /// </summary>
        /// <param name="matric">the key for the dictionary item</param>
        /// <returns>currently returns true regardless as the listboxes are being filled off the dictionary making mistakes impossible</returns>
        public bool removeStudent(int matric)
        {
            studentsOnModule.Remove(matric);
            return true;
        }
        /// <summary>
        /// returns the module leader by getting the payroll number off the staff object
        /// </summary>
        /// <returns>the payroll number</returns>
        public string returnLecturer()
        {
            return leader.returnPayroll().ToString();
        }
        /// <summary>
        /// adds a new item or updates an existing item inside the results dictionary
        /// </summary>
        /// <param name="matric">unique identifier for the dictionary item</param>
        /// <param name="grade">is the mark given from the textbox</param>
        /// <returns>returns true or false depending on success</returns>
        public bool addMark(int matric, string grade)
        {
            results newResult = new results();
            //if it already exists
            if (studentGrades.ContainsKey(matric))
            {
                //try to set mark but if it fails
                if (!studentGrades[matric].setMark(grade))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else//if it doesnt already exist in the dictionary
            {
                //try to set the martic
                if (!newResult.setMatric(matric.ToString()))
                {
                    return false;
                }
                else
                {
                    //set the marks
                    newResult.setMark(grade);
                    studentGrades.Add(matric, newResult);
                    return true;
                }
                
            }
        }
        //returns a single students results
        public results getResult(int matric)
        {
            return studentGrades[matric]; 
        }
        //checks if a student exists in the dictionary
        public bool checkIfExists(int matric)
        { 
            if(studentGrades.ContainsKey(matric))
            {
                return true;
            }
            return false;
        }
    }
}
