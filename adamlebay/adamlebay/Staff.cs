﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adamlebay
{
    /// <summary>
    /// holds the attributes for the staff object. Extends the person object.
    /// </summary>
    public class Staff: Person
    {
        private int payRollNo;
        private string department;
        private string role;

        public bool setPayRollNo(string payrollNo)
        {
            int result;
            bool b = int.TryParse(payrollNo, out result);
            if (b)
            {
                if (result >= 9000 && result <= 9999)
                {
                    //checks if the payroll number already exists inside the list
                    if (listHolder.staffDict.ContainsKey(result))
                    {
                        return false;
                    }
                    else
                    {
                        payRollNo = result;
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool setDepartment(string departmentIn)
        {
            if (checkNotBlank(departmentIn))
            {
                department = departmentIn;
                return true;
            }
            return false;
        }
        public bool setRole(string roleIn)
        {
            if (checkNotBlank(roleIn))
            {
                if (roleIn == "Lecturer" || roleIn == "Senior Lecturer" || roleIn == "Professor")
                {
                    role = roleIn;
                    return true;
                }

            }
            return false;
        }
        public int returnPayroll()
        {
            return payRollNo;
        }
        public string returnDepartment()
        {
            return department;
        }
        public string returnRole()
        {
            return role;
        }

    }
}
