﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace adamlebay
{
    /// <summary>
    /// Interaction logic for StaffEditor.xaml
    /// </summary>
    public partial class StaffEditor : Window
    {
        public StaffEditor()
        {
            InitializeComponent();
            refreshListboxes();

        }
        private void refreshListboxes()
        {
            foreach (KeyValuePair<int, Staff> kvp in listHolder.staffDict)
            {
                staffListBox.Items.Add(kvp.Key.ToString());
            }
        }

        private void addStaffButton_Click(object sender, RoutedEventArgs e)
        {
            Staff newStaff = new Staff();
            if (!newStaff.setName(nameTextbox.Text))
            {
                MessageBox.Show("invalid name");
            }
            else
            {
                if (!newStaff.setAddress(addressTextbox.Text))
                {
                    MessageBox.Show("invalid address");
                }
                else
                {
                    if (!newStaff.setEmail(emailTextbox.Text))
                    {
                        MessageBox.Show("invalid email");
                    }
                    else
                    {
                        if (!newStaff.setPayRollNo(payrollNoTextbox.Text))
                        {
                            MessageBox.Show("invalid payroll");
                        }
                        else
                        {
                            if (!newStaff.setDepartment(departmentTextbox.Text))
                            {
                                MessageBox.Show("invalid department");
                            }
                            else
                            {
                                if (!newStaff.setRole(roleTextbox.Text))
                                {
                                    MessageBox.Show("invalid role");
                                }
                                else
                                {
                                    listHolder.staffDict.Add(newStaff.returnPayroll(), newStaff);
                                    MessageBox.Show("added");
                                    //clear all the textboxes after the add then clear and refill the listbox
                                    nameTextbox.Clear();
                                    departmentTextbox.Clear();
                                    payrollNoTextbox.Clear();
                                    addressTextbox.Clear();
                                    roleTextbox.Clear();
                                    emailTextbox.Clear();
                                    staffListBox.Items.Clear();
                                    refreshListboxes();

                                }
                            }
                        }
                    }
                }
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            moduleListbox.Items.Clear();
            //fills the labels with the details of the staff member selected inside the listbox
            staffNameLabel.Content = listHolder.staffDict[Convert.ToInt32(staffListBox.SelectedItem.ToString())].getName();
            staffPayrollLabel.Content = listHolder.staffDict[Convert.ToInt32(staffListBox.SelectedItem.ToString())].returnPayroll();
            staffAddressLabel.Content = listHolder.staffDict[Convert.ToInt32(staffListBox.SelectedItem.ToString())].getAddress();
            staffDepartmentLabel.Content = listHolder.staffDict[Convert.ToInt32(staffListBox.SelectedItem.ToString())].returnDepartment();
            staffRoleLabel.Content = listHolder.staffDict[Convert.ToInt32(staffListBox.SelectedItem.ToString())].returnRole();
            staffEmailLabel.Content = listHolder.staffDict[Convert.ToInt32(staffListBox.SelectedItem.ToString())].getEmail();

            //searches the dictionary inside the module selected to see if the lecturer teaches that each module. Could be done more efficiently 
            foreach(KeyValuePair<string, Modules> kvp in listHolder.modulesDict)
            {
                if (kvp.Value.returnLecturer() == staffListBox.SelectedItem.ToString())
                {
                    moduleListbox.Items.Add(kvp.Key);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int result;
            bool b = Int32.TryParse(newAssignedStaffTextbox.Text, out result);
            

            if (b)
            {
                if (listHolder.staffDict.ContainsKey(result))
                {
                    listHolder.modulesDict[moduleListbox.SelectedItem.ToString()].setLeader(newAssignedStaffTextbox.Text);
                    MessageBox.Show("complete");
                }
                else
                {
                    MessageBox.Show("invalid staff payroll");
                }

            }
        }


    }
}
