﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adamlebay
{
    /// <summary>
    /// holds common attributes that people working and studying in the college have
    /// </summary>
    public class Person
    {
        private string name;
        private string address;
        private string email;

        //method used to check if a null or empty string has been passed in
        //@param input: this is the value passed in from the textbox
        //returns a boolean, false if the string is null or empty. True if it contains a value
        public bool checkNotBlank(String input)
        {
            if (input == null || input == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        //setters/getters
        public string getName()
        {
            return name;
        }
        
        public bool setName(string value)
        {
            //checks if the textbox is null using the checkNotBlank method
            if (checkNotBlank(value))
            {
                name = value;
                return true;
            }
            else
            {
                return false;
            }
        }
        public string getAddress()
        {
            return address;
        }

        public bool setAddress(string value)
        {
            //checks if the textbox is null using the checkNotBlank method
            if (checkNotBlank(value))
            {
                address = value;
                return true;
            }
            else
            {
                return false;
            }
        }
        public string getEmail()
        {
            return email;
        }

        public bool setEmail(string value)
        {
            //checks for blank value
            if (checkNotBlank(value))
            {
                bool b;
                b = value.Contains("@");
                //checks for the @ symbol inside the textbox to validate email input
                if (b)
                {
                    email = value;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
