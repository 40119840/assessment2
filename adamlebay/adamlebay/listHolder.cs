﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adamlebay
{
    /// <summary>
    /// holds global variables in the form of dictionaries 
    /// </summary>
    public class listHolder
    {
        public static Dictionary<string, Modules> modulesDict = new Dictionary<string, Modules>();
        public static Dictionary<int, Staff> staffDict = new Dictionary<int, Staff>();
        public static Dictionary<int, Students> studentDic = new Dictionary<int, Students>();

    }
}
