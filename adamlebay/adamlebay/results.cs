﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adamlebay
{
    /// <summary>
    /// holds the results for students.
    /// </summary>
     public class results
    {
        private double mark;
        private int studentMatric;
        private string status;

        public bool setMark(string markIN)
        {
            if (markIN != "")
            {
                double result;
                bool b = double.TryParse(markIN, out result);
                if (b)
                {
                    mark = result;
                    //sets the status for the users
                    if (mark < 40)
                    {
                        setStatus("fail");
                    }
                    else
                    {
                        setStatus("pass");
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        public bool setMatric(string matricIN)
        {

            int result;
            bool b = Int32.TryParse(matricIN, out result);
            if (b)
            {
                studentMatric = result;
                return true;
            }
            else
            {
                return false;
            }

        }
        private void setStatus(string StatusIN)
        { 
            status = StatusIN;
        }
        public string returnStatus()
        {
            return status;
        }
        public double returnGrade()
        {
            return mark;
        }
        
     }
}
