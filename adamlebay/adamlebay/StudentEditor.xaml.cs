﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace adamlebay
{
    /// <summary>
    /// Interaction logic for StudentEditor.xaml
    /// </summary>
    public partial class StudentEditor : Window
    {
        public StudentEditor()
        {
            InitializeComponent();
            StudentEditor_Loaded();

        }

        private void StudentEditor_Loaded()
        {
            //fills the listbox with the values from the student list
            foreach (KeyValuePair<int, Students> kvp in listHolder.studentDic)
            {
                studentListBox.Items.Add(kvp.Key.ToString());
            }
        }

        private void Add_student_click(object sender, RoutedEventArgs e)
        {
            //creates a new student to be added 
            Students newStudent = new Students();
            //checks if the setname returns false or true
            if (!newStudent.setName(nameTextbox.Text))
            {
                //if the value in textbox is invalid
                MessageBox.Show("Please enter a name");
            }
            else
            {
                //checks if there is valid data inside the email textbox
                if (!newStudent.setEmail(emailTextbox.Text))
                {
                    MessageBox.Show("invalid email");
                }
                else
                {
                    //the setMatric method checks that the matric is in the correct range and can is an actual integer value
                    if (!newStudent.setMatric(matricTextbox.Text))
                    {
                        MessageBox.Show("invalid matric no");
                    }
                    else
                    {
                        if (!newStudent.setAddress(addressTextbox.Text))
                        {
                            MessageBox.Show("invalid address");
                        }
                        else
                        {
                            try
                            {
                                listHolder.studentDic.Add(newStudent.returnMatric(), newStudent);
                                MessageBox.Show("student added");
                                studentListBox.Items.Clear();
                                StudentEditor_Loaded();
                            }
                            catch
                            {
                                MessageBox.Show("matric registered. To update students click the edit button");
                            }
                           
                            
                        }
                    }
                }
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Students newStudent = new Students();
            newStudent = listHolder.studentDic[Convert.ToInt32(studentListBox.SelectedItem.ToString())];
            nameTextbox.Text = newStudent.getName();
            emailTextbox.Text = newStudent.getEmail();
            addressTextbox.Text = newStudent.getAddress();
            matricTextbox.Text = newStudent.returnMatric().ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //clears all the textboxes once the clear button is pressed
            nameTextbox.Clear();
            emailTextbox.Clear();
            addressTextbox.Clear();
            matricTextbox.Clear();
        }

        private void editStudentButton_Click(object sender, RoutedEventArgs e)
        {
            int result;
            bool b = Int32.TryParse(matricTextbox.Text, out result);
            if (b)
            {
                if (listHolder.studentDic.ContainsKey(result))
                {
                    if (!listHolder.studentDic[result].setName(nameTextbox.Text))
                    {
                        MessageBox.Show("invalid name");
                    }
                    else
                    {
                        if (!listHolder.studentDic[result].setEmail(emailTextbox.Text))
                        {
                            MessageBox.Show("invalid email");
                        }
                        else 
                        {
                            if (!listHolder.studentDic[result].setAddress(addressTextbox.Text))
                            {
                                MessageBox.Show("invalid address");
                            }
                            else
                            {
                                MessageBox.Show("edited");
                            }
                        }
                    }
                   
                    
                }
            }
        }








    }
}
