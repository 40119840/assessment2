﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace adamlebay
{
    /// <summary>
    /// Interaction logic for ModuleEditor.xaml
    /// </summary>
    public partial class ModuleEditor : Window
    {
        public ModuleEditor()
        {
            
            InitializeComponent();
            ModuleEditor_Loaded();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ModuleEditor_Loaded()
        {
            foreach (KeyValuePair<string, Modules> kvp in listHolder.modulesDict)
            {
                moduleLb.Items.Add(kvp.Key.ToString());
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Modules newModule = new Modules();
            if (!newModule.setName(nameTextbox.Text))
            {
                MessageBox.Show("invalid name");
            }
            else
            {
                if (!newModule.setCode(codeTextbox.Text))
                {
                    MessageBox.Show("invalid code");
                }
                else
                { 
                    if(!newModule.setLeader(lecturerTextbox.Text))
                    {
                        MessageBox.Show("invalid leader payroll");
                    }
                    else
                    {
                        listHolder.modulesDict.Add(newModule.returnCode(), newModule);
                        MessageBox.Show("added");
                        ModuleEditor_Loaded();
                    }
                }
            }
        }

        private void moduleLb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //fills the labels to show details of the course by getting the values in the dictionary using the key provided from the listbox selected value
            courseNameLabel.Content = listHolder.modulesDict[moduleLb.SelectedItem.ToString()].returnName();
            courseLeaderLabel.Content = listHolder.modulesDict[moduleLb.SelectedItem.ToString()].returnLecturer();
            
            if (listHolder.modulesDict.ContainsKey(moduleLb.SelectedItem.ToString()))
            {
                studentLb.Items.Clear();
                Modules dict = listHolder.modulesDict[moduleLb.SelectedItem.ToString()];
                Dictionary<int, Students> enrolled  = dict.returnStudentsOnModule();
                foreach (KeyValuePair<int, Students> kvp in enrolled)
                {
                    studentLb.Items.Add(kvp.Key.ToString());
                  
                }
            }
            



        }

        private void addStudentButton_Click(object sender, RoutedEventArgs e)
        {
            
            //Modules dict = listHolder.modulesDict[moduleLb.SelectedItem.ToString()];
            int result;
            bool b = Int32.TryParse(matricNoTextbox.Text, out result);
            if(b)
            {
                try
                {
                    Students studentIN = listHolder.studentDic[result];
                    listHolder.modulesDict[moduleLb.SelectedItem.ToString()].addStudent(studentIN.returnMatric(), studentIN);
                    MessageBox.Show("complete");
                    ModuleEditor_Loaded();
                }
                catch(KeyNotFoundException)
                {
                    MessageBox.Show("matric not found");
                }

            }
        }

        private void removeStudentFromModuleButton_Click(object sender, RoutedEventArgs e)
        {
            listHolder.modulesDict[moduleLb.SelectedItem.ToString()].removeStudent(Convert.ToInt32(studentLb.SelectedItem.ToString()));
            MessageBox.Show("Removed");
                        
        }

        private void changeLecturer_Click(object sender, RoutedEventArgs e)
        {
            int result;
            bool b = Int32.TryParse(staffPayrollNoChange.Text, out result);

            if (b)
            {
                if (listHolder.staffDict.ContainsKey(result))
                {
                    listHolder.modulesDict[moduleLb.SelectedItem.ToString()].setLeader(staffPayrollNoChange.Text);
                    MessageBox.Show("changed");
                }

            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            // edits/adds the students mark
            if (!listHolder.modulesDict[moduleLb.SelectedItem.ToString()].addMark(Convert.ToInt32(studentLb.SelectedItem.ToString()), gradeTextbox.Text))
            {
                MessageBox.Show("invalid grade");
            }
            else
            {
                MessageBox.Show("Added");
            }
        }

        private void studentLb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (listHolder.modulesDict[moduleLb.SelectedItem.ToString()].checkIfExists(Convert.ToInt32(studentLb.SelectedItem.ToString())))
                {
                    results resultFiller = listHolder.modulesDict[moduleLb.SelectedItem.ToString()].getResult(Convert.ToInt32(studentLb.SelectedItem.ToString()));

                    gradeLabel.Content = resultFiller.returnGrade();
                    statusLabel.Content = resultFiller.returnStatus();
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("please select an option");
            }

  
        }






    }
}
